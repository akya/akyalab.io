import { defineConfig } from 'astro/config';

export default defineConfig({
  sitemap: true,
  site: 'https://akya.gitlab.io/',
  outDir: 'public',
  publicDir: 'static',
});
